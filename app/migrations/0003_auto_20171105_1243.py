# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-05 12:43
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_urls'),
    ]

    operations = [
        migrations.RenameField(
            model_name='films',
            old_name='film_desc',
            new_name='desc',
        ),
        migrations.RenameField(
            model_name='films',
            old_name='film_img',
            new_name='img',
        ),
        migrations.RenameField(
            model_name='films',
            old_name='film_name',
            new_name='name',
        ),
    ]
