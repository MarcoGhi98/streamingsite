# Generated by Django 2.0 on 2017-12-16 15:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0014_urls_source_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='films',
            name='views',
            field=models.IntegerField(default=0),
        ),
    ]
