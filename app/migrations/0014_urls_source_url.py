# Generated by Django 2.0 on 2017-12-16 15:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0013_auto_20171128_1510'),
    ]

    operations = [
        migrations.AddField(
            model_name='urls',
            name='source_url',
            field=models.URLField(default=''),
        ),
    ]
