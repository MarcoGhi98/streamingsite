from django.http import HttpResponse
from django.template import loader
from app.models import Films, Categories, Urls
from app.classes.utils import Utils
from django.core import serializers
import subprocess, json


def index(request):
    films = Films.objects.all().order_by('created_at')[:84]
    for film in films:
        film.title = film.title.upper()
        film.rating = int((film.rating * 5) / 10)

    # with connection.cursor() as cursor:
        # cursor.execute("SELECT app_films.*, app_urls.film_url FROM app_films, app_urls WHERE app_urls.film_id_id = app_films.id")
        # results = dictfetchall(cursor)

    context = {
        'home_films': films,
        'categories': Categories.objects.all(),
    }

    return HttpResponse(loader.get_template("app/index.html").render(context, request))


def search(request, s):
    films = Films.objects.filter(title__contains=s).order_by('views')[:20]
    for film in films:
        film.desc = Utils.smart_truncate(film.desc)

    return HttpResponse(serializers.serialize('json', films), content_type='application/json')


'''def get_film(request, s):
    bot = Bot()
    results = bot.search_from_cb01(s)
    return HttpResponse(serializers.serialize('json', results), content_type='application/json')'''


def category(request, cat_id):

    '''with connection.cursor() as cursor:
        cursor.execute("SELECT app_films.* FROM app_categories, app_films WHERE app_films.cat_id_id = app_categories.id AND app_categories.id = %s", [cat_id])
        results = dictfetchall(cursor)'''

    films = Films.objects.filter(cat_id_id=cat_id).order_by('created_at','views')[:30]
    for film in films:
        film.desc = Utils.smart_truncate(film.desc)

    context = {
        'cat_films': films,
        'selected_category': Categories.objects.get(pk=cat_id),
    }

    return HttpResponse(loader.get_template("app/category.html").render(context, request))


def play(request, film_id):
    '''urls =
    videos = []

    for url in urls:
        # page = BeautifulSoup(requests.get(url.film_url).content, "html.parser")
        videos.append(requests.get(url.film_url).text)'''

    current_film = Films.objects.get(pk=film_id)
    Films.objects.filter(pk=film_id).update(views=current_film.views + 1)
    suggested_films = Films.objects.filter(cat_id_id=current_film.cat_id).exclude(pk=film_id)[:5]

    for film in suggested_films:
        film.desc = Utils.smart_truncate(film.desc, length=100)

    context = {
        'stream_urls': serializers.serialize('json', Urls.objects.filter(film_id=film_id, provider='Openload')),
        'selected_film': current_film,
        'film_category': Categories.objects.get(pk=current_film.cat_id_id),
        'suggested_films': suggested_films,
    }

    return HttpResponse(loader.get_template("app/play.html").render(context, request))


def get_source(request, film_id):
    urls = Urls.objects.filter(film_id=film_id, provider='Openload')
    source_url = []
    for url in urls:
        with subprocess.Popen(["youtube-dl", "-g", url.film_url], stdout=subprocess.PIPE) as proc:
            source_url.append(str(proc.stdout.read()).replace("b'","").replace("\\n'",""))

        '''f = io.StringIO()
        with redirect_stdout(f):
            subprocess.run(["youtube-dl", "-g", url.film_url], stdout=subprocess.PIPE)
        out = f.getvalue()'''

    # return HttpResponse(source_url, content_type="application/json")
    return HttpResponse(json.dumps(source_url), "application/json")


def dictfetchall(cursor):
    # "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
